package com.example.kotlinCoroutine.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinCoroutine.App
import com.example.kotlinCoroutine.R

class CustomAdaptor(private val context: Context, apps: List<App>) :
    RecyclerView.Adapter<CustomAdaptor.MyViewHolder>() {
    private val apps: List<App>

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var mName: TextView
        var mSize: TextView
        var mImage: ImageView

        init {
            mName = itemView.findViewById(R.id.name)
            mSize = itemView.findViewById(R.id.size)
            mImage = itemView.findViewById(R.id.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v: View = LayoutInflater.from(context).inflate(R.layout.layout_list_item, parent, false)
        return MyViewHolder(v)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val app: App = apps[position]
        holder.mName.setText(app.getName())
        holder.mSize.setText(app.getSize().toString() + " MB")
        holder.mImage.setImageResource(app.getImage())
    }

    override fun getItemCount(): Int {
        return apps.size
    }

    init {
        this.apps = apps
    }
}