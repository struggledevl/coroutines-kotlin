package com.example.kotlinCoroutine

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.kotlinCoroutine.`object`.discoverWithGenres.DiscoverMovie
import com.example.kotlinCoroutine.`object`.movieDetails.MovieResultDetail
import com.example.kotlinCoroutine.`object`.popularMovie.MovieResults
import com.example.kotlinCoroutine.adapter.CustomAdaptor
import com.example.kotlinCoroutine.inteface.discoverMovies
import com.example.kotlinCoroutine.inteface.movieDetails
import com.example.kotlinCoroutine.inteface.popularMovie
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.*

//class MainActivity : AppCompatActivity() {
//
//    var mList1: RecyclerView? = null
//    var mList2: RecyclerView? = null
//    var appList: MutableList<App>? = null
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//
//        mList1 = findViewById(R.id.list_action)
//        mList2 = findViewById(R.id.list_comedy)
//        appList = ArrayList()
//        appList?.add(App(R.drawable.youtube, "Youtube", 40))
//        appList?.add(App(R.drawable.maxplayer, "Max Player", 30))
//        appList?.add(App(R.drawable.messenger, "Messenger", 20))
//        appList?.add(App(R.drawable.twitter, "Twitter", 22))
//        appList?.add(App(R.drawable.vlc, "VLC Player", 40))
//        appList?.add(App(R.drawable.whatsapp, "Whatsapp", 34))
//        val manager1 = LinearLayoutManager(this)
//        manager1.orientation = LinearLayoutManager.HORIZONTAL
//        mList1?.layoutManager = manager1
//        val manager2 = LinearLayoutManager(this)
//        manager2.orientation = LinearLayoutManager.HORIZONTAL
//        mList2?.layoutManager = manager2
//        val adaptor1 = CustomAdaptor(this, appList as ArrayList<App>)
//        mList1?.adapter = adaptor1
//        val adaptor2 = CustomAdaptor(this, appList as ArrayList<App>)
//        mList2?.adapter = adaptor2
//    }
//}

class MainActivity : AppCompatActivity() {

    /**
     * companion object
     * */
    companion object {

        /**
         * Declaration variables
         * */

        /**
         * interface variables
         * */
        var PAGE = 1
        var BASE_URL = "https://api.themoviedb.org"
        var API_KEY = "8dcf4efe6c20ee38917db4f3e436daee"
        var LANGUAGE = "en-US"
        var PATH = "movie"
        var CATEGORY = "popular"
        const val SORT_BY: String = "popularity.desc"

        /**
         * genres id
         * */
        private val WITH_GENRES: IntArray = intArrayOf(28, 35)
        const val COMEDY: Int = 35
        private const val ACTION: Int = 28

        val arr = IntArray(10)
        val titleArray = arrayOfNulls<String>(10)

        val popularArr = IntArray(10)
        val titlePopularArray = arrayOfNulls<String>(10)

        /**
         * TextView title
         * */
        private var titleText1: TextView? = null
        private var titleText2: TextView? = null
        private var titleText3: TextView? = null

        /**
         * TextView description
         * */
        private var descriptionText1: TextView? = null
        private var descriptionText2: TextView? = null
        private var descriptionText3: TextView? = null

        /**
         * Button
         * */
        private var tapButton: Button? = null

        /**
         * Retrofit variables popular movie
         * */
        var popularMovie: Retrofit? = null
        var popularInterface: popularMovie? = null

        /**
         * Retrofit variables discover movies
         * */
        var discoverMovies: Retrofit? = null
        var discoverInterface: discoverMovies? = null

        /**
         * Retrofit variables movie details
         * */
        var movieDetails: Retrofit? = null
        var movieDetailsInterface: movieDetails? = null
    }

    /**
     * onCreate lifecycle
     * */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // to change title of activity
        // val actionBar = supportActionBar

        /**
         * initialize title
         * */
        titleText1 = findViewById<View>(R.id.title1) as TextView
        titleText2 = findViewById<View>(R.id.title2) as TextView
        titleText3 = findViewById<View>(R.id.title3) as TextView

        /**
         * initialize description
         **/
        descriptionText1 = findViewById<View>(R.id.description1) as TextView
        descriptionText2 = findViewById<View>(R.id.description2) as TextView
        descriptionText3 = findViewById<View>(R.id.description3) as TextView

        /**
         * initialize tap button
         **/
        tapButton = findViewById<View>(R.id.tap) as Button

        /**
         * ClickListener Method
         **/
        tapButton!!.setOnClickListener {
            GlobalScope.async {
                println("tap button setOnClickListener")
                getDiscoverMovie()
                fetchDiscoverComedy()
            }.onAwait
        }
    }

    /**
     * retrofit function for Popular Movies
     **/
    private suspend fun getPopularMovie() {
        withContext(CoroutineScope(Dispatchers.IO).coroutineContext) {
            popularMovie = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            popularInterface =
                popularMovie?.create(com.example.kotlinCoroutine.inteface.popularMovie::class.java)
        }
    }

    /**
     * retrofit function for Discover Movies
     **/
    private suspend fun getDiscoverMovie() {
        withContext(CoroutineScope(Dispatchers.IO).coroutineContext) {
            discoverMovies = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            discoverInterface =
                discoverMovies?.create(com.example.kotlinCoroutine.inteface.discoverMovies::class.java)
        }
    }

    /**
     * retrofit function for Movie Details
     **/
    private suspend fun getMovieDetails() {
        withContext(CoroutineScope(Dispatchers.IO).coroutineContext) {
            movieDetails = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            movieDetailsInterface =
                movieDetails?.create(com.example.kotlinCoroutine.inteface.movieDetails::class.java)
        }
    }

    /**
     * fetch popular movie
     **/
    private suspend fun fetchPopularMovie() {
        CoroutineScope(IO).async {
            popularInterface?.listPopularMovies(CATEGORY, API_KEY, LANGUAGE, PAGE)
                ?.enqueue(object : Callback<MovieResults?> {
                    @SuppressLint("SetTextI18n")
                    override fun onResponse(
                        call: Call<MovieResults?>, response: Response<MovieResults?>
                    ) {
                        /**
                         * Response API popularInterface
                         * */

                        println("test fetch api popular movie")

                        /**
                         * change title action bar
                         * */
//                        actionBar!!.title = "Fetch Popular Movie"

                        // declaration value results as response.body() from API
                        val results = response.body()

                        // declaration value as results
                        val listOfMovies = results!!.results

                        Log.d("TAG", "Movie size ${listOfMovies.size}")

                        // declaration value rand as Random() method
                        val rand = Random()

                        // declaration value randMovies from random int listOfMovies
                        val randMovies = rand.nextInt(listOfMovies.size - 10)

                        Log.d("popular_size", "${listOfMovies.size}")

                        /**
                         * looping k, for insert movie id from popular movie
                         * */
                        for (k in 0..9) {

                            //
                            arr[k] = listOfMovies[randMovies + k].id
                            titleArray[k] = listOfMovies[randMovies + k].title

                            /**
                             * output Log random popular movie & movie id
                             * */
//                            println("Title Movies ke $k : ${titleArray[k]}")
//                            println("random movie ${k + 1}: ${randMovies + k}")
//                            println("movie id ke k ${k + 1} : ${arr[k]}")
                        }

                        CoroutineScope(IO).async {
                            println("fetch movie detail with coroutine")
                            getMovieDetails()
                            fetchMovieDetail()
                        }.onAwait
                    }

                    override fun onFailure(call: Call<MovieResults?>, t: Throwable) {
                        t.printStackTrace()
                    }
                })
        }.await()
    }

    /**
     *  fetch discover movie
     **/
    private suspend fun fetchDiscoverComedy() {
        for (k in 0..1) {
            CoroutineScope(IO).async {
                // variable genres for genres id
                var genres: Int = 0

                if (k == 0) {
                    genres = ACTION
                } else {
                    genres = COMEDY
                }

                discoverInterface?.listDiscoverMovies(
                    PATH,
                    API_KEY,
                    LANGUAGE,
                    SORT_BY,
                    PAGE,
                    genres
                )?.enqueue(object : Callback<DiscoverMovie?> {
                    override fun onResponse(
                        call: Call<DiscoverMovie?>,
                        response: Response<DiscoverMovie?>
                    ) {
                        /**
                         * Response API popularInterface
                         * */

                        println("test fetch api discover movie")

                        /**
                         * change title action bar
                         * */

////                    actionBar!!.title = "Fetch Discover With Genres"

                        // declaration value results as response.body() from API
                        val results = response.body()

                        // declaration value as results
                        val listOfMovies = results!!.results

                        for (i in 0..2) {
                            // print array popular with genres title and overview
                            println("discover with genres title : ${listOfMovies?.get(i)?.title}")
                            println("discover with genres overview : ${listOfMovies?.get(i)?.overview}")

                            if (i == 0) {
                                titleText1!!.text = listOfMovies?.get(0)?.title
                                descriptionText1!!.text = listOfMovies?.get(0)?.overview
                            } else if (i == 1) {
                                titleText2!!.text = listOfMovies?.get(1)?.title
                                descriptionText2!!.text = listOfMovies?.get(1)?.overview
                            } else if (i == 2) {
                                titleText3!!.text = listOfMovies?.get(2)?.title
                                descriptionText3!!.text = listOfMovies?.get(2)?.overview
                            }
                        }
                    }

                    override fun onFailure(call: Call<DiscoverMovie?>, t: Throwable) {
                        TODO("Not yet implemented")
                    }
                })
            }.await()
        }
    }

    /**
     * fetch movie detail
     * */
    private suspend fun fetchMovieDetail(
        arrayMovieId: IntArray = intArrayOf(
            arr[0],
            arr[1],
            arr[2],
        )
    ) {
        for (i in 0..2) {
            CoroutineScope(IO).async {
                movieDetailsInterface?.listMovieDetails(
                    arrayMovieId[i],
                    API_KEY,
                    LANGUAGE
                )
                    ?.enqueue(object : Callback<MovieResultDetail?> {
                        @SuppressLint("SetTextI18n")
                        override fun onResponse(
                            call: Call<MovieResultDetail?>,
                            response: Response<MovieResultDetail?>
                        ) {

                            /**
                             * Response API movieDetailsInterface
                             * */

                            println("test fetch movie detail")

                            /**
                             * change title action bar
                             * */
//                            actionBar!!.title = "Fetch Movie Details"

                            val movieDetailResults = response.body()

                            println("fetch movie details: $movieDetailResults")

                            if (i == 0) {
                                titleText1!!.text = movieDetailResults?.title
                                descriptionText1!!.text = movieDetailResults?.overview
                            } else if (i == 1) {
                                titleText2!!.text = movieDetailResults?.title
                                descriptionText2!!.text = movieDetailResults?.overview
                            } else if (i == 2) {
                                titleText3!!.text = movieDetailResults?.title
                                descriptionText3!!.text = movieDetailResults?.overview
                            }
                        }

                        override fun onFailure(call: Call<MovieResultDetail?>, t: Throwable) {
                            t.printStackTrace()
                        }
                    })
            }.await()
        }
    }
}