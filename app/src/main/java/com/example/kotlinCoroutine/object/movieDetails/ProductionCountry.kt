package com.example.kotlinCoroutine.`object`.movieDetails

data class ProductionCountry(
    val iso_3166_1: String,
    val name: String
)