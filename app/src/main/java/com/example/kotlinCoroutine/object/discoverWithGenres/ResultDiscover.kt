package com.example.kotlinCoroutine.`object`.discoverWithGenres

data class ResultDiscover(
    /**
     * adult : false
     * backdrop_path : /ckfwfLkl0CkafTasoRw5FILhZAS.jpg
     * genre_ids : [28,35,14]
     * id : 602211
     * original_language : en
     * original_title : Fatman
     * overview : A rowdy, unorthodox Santa Claus is fighting to save his declining business. Meanwhile, Billy, a neglected and precocious 12 year old, hires a hit man to kill Santa after receiving a lump of coal in his stocking.
     * popularity : 1848.361
     * poster_path : /4n8QNNdk4BOX9Dslfbz5Dy6j1HK.jpg
     * release_date : 2020-11-13
     * title : Fatman
     * video : false
     * vote_average : 6.1
     * vote_count : 118
     */
    var isAdult: Boolean = false,
    var backdrop_path: String? = null,
    var id: Int = 0,
    var original_language: String? = null,
    var original_title: String? = null,
    var overview: String? = null,
    var popularity: Double = 0.0,
    var poster_path: String? = null,
    var release_date: String? = null,
    var title: String? = null,
    var isVideo: Boolean = false,
    var vote_average: Double = 0.0,
    var vote_count: Int = 0,
    var genre_ids: List<Int>? = null
)