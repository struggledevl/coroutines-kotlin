package com.example.kotlinCoroutine.`object`.movieDetails

data class Genre(
    val id: Int,
    val name: String
)