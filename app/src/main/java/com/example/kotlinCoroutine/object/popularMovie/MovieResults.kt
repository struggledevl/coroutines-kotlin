package com.example.kotlinCoroutine.`object`.popularMovie

data class MovieResults(
    val page: Int,
    val results: List<Result>,
    val total_pages: Int,
    val total_results: Int
)