package com.example.kotlinCoroutine.inteface

import com.example.kotlinCoroutine.`object`.movieDetails.MovieResultDetail
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Get movie details interface
 * */
interface movieDetails {
    @GET("/3/movie/{movie_id}")
    fun listMovieDetails(
        @Path("movie_id") movieId: Int,
        @Query("api_key") apiKey: String?,
        @Query("language") language: String?
    ): Call<MovieResultDetail?>?
}