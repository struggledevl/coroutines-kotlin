package com.example.kotlinCoroutine.inteface

import com.example.kotlinCoroutine.`object`.discoverWithGenres.DiscoverMovie
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Get discover movies interface
 * */
interface discoverMovies {
    @GET("/3/discover/{path}")
    fun listDiscoverMovies(
        @Path("path") path: String?,
        @Query("api_key") apiKey: String?,
        @Query("language") language: String?,
        @Query("sort_by") sortBy: String?,
        @Query("page") page: Int,
        @Query("with_genres") withGenres: Int
    ): Call<DiscoverMovie?>?
}