package com.example.kotlinCoroutine.inteface

import com.example.kotlinCoroutine.`object`.popularMovie.MovieResults
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Get popular movies interface
 * */
interface popularMovie {
    @GET("/3/movie/{category}")
    fun listPopularMovies(
        @Path("category") category: String?,
        @Query("api_key") apiKey: String?,
        @Query("language") language: String?,
        @Query("page") page: Int
    ): Call<MovieResults?>?
}
